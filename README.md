# Esercizio Javascript e Jquery

Attraverso javascript (è possibile usare, se e quando voluto, anche qualsiasi metodo [jQuery](https://api.jquery.com/)) mostrare una lista di utenti.  
Ogni utente deve essere visualizzato in una "card" che ne mostri nome, username ed email.  
Ogni card sarà collegata ad un evento click il quale mostrerà nel lato destro della pagina una sezione nella quale verranno caricati e visualizzati i post degli utenti.  
I dati verrano reperiti tramite questo portale [https://jsonplaceholder.typicode.com/](https://jsonplaceholder.typicode.com/)

**Bonus tasks:**  

*   Usare [Bootstrap](https://getbootstrap.com/docs/3.4/) per dare uno style al layout
*   Ogni post sarà cliccabile. In quel caso verrà aperta una modale di bootstrap nel quale si vedrà il dettaglio del post e i commenti associati
*   Nella card degli user mostrare i dettagli dell'utente (website, company details) usando la funzione collapse di Bootstrap.
*   Nei dettagli nascosti dello user usare le informazioni di address per embeddare una [google maps](https://developers.google.com/maps/documentation/embed/get-started) che punti all'indirizzo dello user